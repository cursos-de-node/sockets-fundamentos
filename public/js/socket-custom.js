var socket = io();

//Escuchar (on)
socket.on('connect', function() {
    console.log('Conectado al servidor');
});

socket.on('disconnect', function() {
    console.log('Perdimos coneccion con el servidor');
});

socket.on('EnviarMensaje', function(mensaje) {
    console.log('Servidor: ', mensaje);
});

//Enviar informacion(emit)
socket.emit('EnviarMensaje', {
    usuario: 'Miguel',
    mensaje: 'Hola mundo'
}, function(resp) {
    console.log('Servidor: ', resp);
});